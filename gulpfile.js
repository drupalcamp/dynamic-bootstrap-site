const { src, dest, parallel } = require('gulp');
const gulp = require('gulp');
// const pug = require('gulp-pug');
const sass = require('gulp-sass');
const minifyCSS = require('gulp-csso');
const concat = require('gulp-concat');
const browserSync = require('browser-sync').create();
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
// function html() {
//     return src('./**/*.php')
//         .pipe(dest('./'))
// }


// BrowserSync Reload
function browserSyncReload(done) {
  browsersync.reload();
  done();
}

function css() {
  return src([
    'node_modules/bootstrap/scss/bootstrap.scss',
    'node_modules/animate.css/animate.css',
    'src/scss/*.scss'
  ])
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(sass({
      outputStyle: 'expanded',
    }).on('error', sass.logError))
    // .pipe(minifyCSS())
    .pipe(autoprefixer('last 2 versions'))
    .pipe(sourcemaps.write('./'))
    .pipe(dest('src/css'))
    .pipe(browserSync.stream());
}

function js() {
  return src([
    'node_modules/bootstrap/dist/js/bootstrap.min.js',
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/popper.js/dist/umd/popper.min.js',
    'src/assets/*.js'
  ], { sourcemaps: true })
    // .pipe(concat('app.min.js'))
    .pipe(sourcemaps.write('./'))
    .pipe(dest('src/js'))
    .pipe(browserSync.stream());
}

function watchFiles() {
  gulp.watch("./src/scss/**/*", css);
  gulp.watch("./src/assets/*", js);
}

exports.js = js;
exports.css = css;
// exports.html = html;
exports.default = gulp.series(parallel(css, js));
exports.watch = watchFiles;