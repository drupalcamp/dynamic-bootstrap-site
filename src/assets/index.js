//  console.log("Test index.js.");

$(document).ready(function() {


/* Nav icon change */
  let $button = $('button.navbar-toggler');

  $button.on('click', function(){
    $(this).parent('.brand-navbar-icon').toggleClass("opened");
    $(this).children('.navbar-toggler-icon').toggleClass('opened');
  });


  let aHover = '.navbar-nav .nav-item>a.nav-link';
  let targetHover = '.menu-hover-right>.hover-box-wapper';
  let $aLinks = $(aHover);

  let aSubHover = '.navbar-nav .nav-item .dropdown-menu .nav-item>a.nav-link';
  let targetSubHover = '.menu-hover-right>.scripts.hover-box-wapper';
  let $aSubLinks = $(aSubHover);

  $aLinks.hover(
    function() {
      menuAnimate($(this), targetHover);
  },
    function() {
      menuFadeout($(this), targetHover);
    }
  );

  $aSubLinks.hover(

    function () {
      // console.log('sub link.');
      menuAnimateSub($(this), targetSubHover);
    },
    function () {
      menuFadeout($(this), targetSubHover);
      subMenuFadeout($(this), '.menu-hover-right>.scripts.hover-box-wapper>.submenu-img');
    }
  );

  /* Indicator Effect */

  let $thumbImg = $('#homeSlideShow .carousel-indicators > li');
  let $thumbActive = $('#homeSlideShow .carousel-indicators > li.active');

  $thumbActive.hide(300);
  $thumbImg.click(function() {
    $(this).hide('slow', function() {
      $thumbImg.not(this).show('slow');
    });
  });

  /*
  * All custom functions called in this script
  */

  function menuAnimate($this, targetHover) {
    className = $this.attr('class').split(" ")[0];
    target = targetHover + "." + className; //vivaldi
    //  console.log(target);
    $(targetHover).stop().hide();

    if(className != 'scripts') {
      $(target).fadeIn('normal').animate({
        // fontSize: "1.4em",
        // borderWidth: "10px",
        left: getRandomInt(-20, 20),
        top: getRandomInt(-20, 20)
      },
      400);
    }
  }

  function menuAnimateSub($this, targetHover) {
    $(targetHover).fadeIn('normal');
    // console.log(targetHover);
    className = $this.attr('class').split(" ")[0];
    target = targetHover + ">.submenu-img." + className; //mrh
    // console.log(target);
    $(target).fadeIn('normal').animate({
        left: getRandomInt(-10, 10),
        top: getRandomInt(-10, 30)
      },
      400);

 
      
  }

  function menuFadeout ($this, targetHover) {
    $(targetHover).fadeOut('fast');
  }

  function subMenuFadeout($this, targetHover) {
    className = $this.attr('class').split(" ")[0];
    target = targetHover + "." + className; //mrh
    $(target).hide();
  }


  function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }


  /* Lasy loading youtube video */

  $('a.play-youtube-img').click(function (event) {
    event.preventDefault();
    id = $(this).attr("data-key");
    url = $(this).attr('href');
    // console.log("id " + id + ": url " + url);

    videoHtml = '<div class="embed-responsive embed-responsive-16by9"> ' + 
      '<iframe clasiframes="embed-responsive-item" ' + 
      'src="https://www.youtube.com/embed/' + id + '?autoplay=1" ' +
      'allowfullscreen allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"></iframe></div>';

    // $(this).replaceWith(videoHtml);
    if ($(this).children('.embed-responsive iframe').length <= 0) {
      $(this).children('img').hide();
      $(this).append(videoHtml);
    }
    else {
      $(this).children('img').show();
    }

  });

  /** Click title link to remove iframe youtube */
  $('.accordion .btn-link').click(function (event) {

    $('.card-body .play-youtube-img .embed-responsive').remove();
    $('.card-body .play-youtube-img img').show();
  
  });

  /** Footer scroll to show "Back to top" button */

  $("#myBtn").click(function(){
    // document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  });

  window.onscroll = function(){
    if ($('body,html').scrollTop() > 40) {
      $("#myBtn").fadeIn('slow');
    } else {
      $("#myBtn").fadeOut('slow');
    }
  }

  // function scrollFunction () {
  //   // console.log($('body,html').scrollTop());

  // }

});