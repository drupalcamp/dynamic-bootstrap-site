<div class="region-copyright container pt-4">
  <div class="clearFix"></div>
  <div class="d-flex">

  <ul class="borderless list-inline mx-auto justify-content-center">
    <li class="list-group-item bg-light">
    <a class="btn-floating btn-lg btn-yt" type="button" role="button"
    href="https://www.youtube.com/channel/UCI9UQIXiP-BYqnFnup-QlTA" aria-label="test" title="youtube">
      <i class="fab fa-youtube"></i>
    </a>
      </li>
    <li class="list-group-item bg-light">
    <a class="btn-floating btn-lg btn-fb" type="button" role="button" 
    href="https://www.facebook.com/gung.wang.us" aria-label="test" title="facebook">
      <i class="fab fa-facebook-f"></i>
    </a>
        </li>
    <li class="list-group-item bg-light">
    <a class="btn-floating btn-lg btn-tw" type="button" role="button" 
    href="https://www.twitter.com/gungwang_ny" aria-label="test" title="twitter">
      <i class="fab fa-twitter"></i>
    </a>
        </li>
    <li class="list-group-item bg-light">
    <a class="btn-floating btn-lg btn-yt" type="button" role="button" 
    href="https://www.youtube.com/watch?v=315qpTafdeQ">
      <i class="fab fa-youtube"></i>
    </a>
        </li>
    <li class="list-group-item bg-light">
    <a class="btn-floating btn-lg btn-li" type="button" role="button" 
    href="https://www.linkedin.com/in/gung-wang/">
      <i class="fab fa-linkedin-in"></i>
    </a>
        </li>
    </ul>

  </div>
  <div class="clearFix"></div>
  <div class="mx-auto justify-content-center footer-copyright text-center py-3">
      Copyright © 2010-2030 Gung Wang Demo Site 
</div>

</div>
