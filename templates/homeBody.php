<div class="m-4"></div>
<div class="container-fluid d-flex flex-wrap">
  <div class="row">

<?php
  // require_once ("templates/variables.php");

  $count = 0;
  $wowEffect=[
    'slideInLeft',
    'slideInRight',
    'fadeIn',
    'bounceInUp',
    'rollIn'
  ];


  foreach($title as $key => $value) {
    $delay = rand(2, 10)/10;
    $effect = $wowEffect[rand(0,4)];
    //yt--8IKDHjtTZio
    $str ='
    <div class="col-sm-6 col-lg-4  wow '. $effect .'" data-wow-delay="' . $delay .'s">
    <div class="card bg-light mb-5 rounded shadow-lg">
      <div class="card-body">
        <div class="img-wrapper mb-4">
          <img class="card-img-top" src="./images/' . $key . '-thumb.jpg" alt="' . $value . '">
        </div>
        <h5 class="card-title mt-4">' . $value . '</h5>
        <p class="card-text">' . $summary[$key] . '  
        </p>
        <a href="' . $urls[$key] . '" class="btn btn-info">Read More</a>

      </div>
    </div>    
    </div>';

    if($count == 0 || $count > 9){
      echo "";
    } 
    else {
      echo $str;
    }

    $count++;
  }
  
  
  
  ?>

</div>

</div>

