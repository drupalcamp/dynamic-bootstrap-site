<nav class="navbar fixed-top navbar-expand-lg navbar-dark .bg-info subpage-menu">
    <a class="navbar-brand" href="/">Home</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#subNavMenu" aria-controls="subNavMenu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>

  <div class="collapse navbar-collapse" id="subNavMenu">

        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

          <li class="nav-item">
            <a class="vivaldi nav-link" href="<?php echo $urls['vivaldi']; ?>"><?php echo $shortTitle['vivaldi']; ?> <span class="sr-only">(current)</span></a>
          </li>

          <li class="nav-item">
            <a class="shakespeare nav-link " href="<?php echo $urls['shakespeare']; ?>">
              <?php echo $shortTitle['shakespeare']; ?></a></li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle " href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Others</a>

              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a href="<?php echo $urls['mrh']; ?>" class="mrh dropdown-item"><?php echo $shortTitle['mrh']; ?></a>

                    <a href="<?php echo $urls['cold']; ?>" class="cold dropdown-item"><?php echo $shortTitle['cold']; ?></a>

                    <a href="<?php echo $urls['juarez']; ?>" class="juarez dropdown-item"><?php echo $shortTitle['juarez']; ?></a>
              </div>
          </li>
          <li class="nav-item">
            <a class="marc nav-link" href="<?php echo $urls['marc']; ?>"><?php echo $shortTitle['marc']; ?></a>
            </li>
        </ul>

        <!-- END Hover Right Box to show image & submenu  -->

  </div>
</nav>
    <!--
      <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
    -->