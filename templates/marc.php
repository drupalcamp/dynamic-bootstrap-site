<div class="banner jumbotron <?php echo $bgClass[$_GET['q']];?>">
  <div class="clearfix align-top">
    <h2 class="title top-title"><?php echo $summary['home'];?></h2>
  </div>
</div>

<div class="heror <?php echo $bgClass[$_GET['q']];?> align-bottom">
  <div class="container">
    <h1 class="title"><?php echo $title[$_GET['q']];?>  </h1>
  </div>

  <div class="read-script subscribe">
    <a href="<?php echo $subscribe;?>" class="read-script-lin eldor-sub-alink">Subscribe Youtube
    </a>
  </div>

</div>

<article class="main-content mt-3">

  <!-- Collaps/fold -->
  <div class="container d-flex h-100">
    <div class="row justify-contnt-center aligh-self-center">

      <div class="body text-justify">

      <p>
      <a class="btn btn-light d8" data-toggle="collapse" href="#img-1" role="button" aria-expanded="false" aria-controls="img-1">
        Acquia Certified Front End Specialist - Drupal 8</a>
      </p>
      <div class="collapse enlarge  mb-3" id="img-1"><img src="./images/marc-home.jpg" alt="Fron End Specialist" class="certificate">
      </div>
      <p>
      <a class="btn btn-light d8" data-toggle="collapse" href="#img-2" role="button" aria-expanded="false" aria-controls="img-2">
        Acquia Certified Developer - Drupal 8</a>
      <div class="collapse enlarge mb-3" id="img-2"><img src="./images/marc-home.jpg" alt="Certified Developer" class="certificate">
      </div>
      </p>
      <a class="btn btn-light d8" data-toggle="collapse" href="#img-3" role="button" aria-expanded="false" aria-controls="img-3">Acquia Certified Site Builder - Drupal 8</a>
      <div class="collapse enlarge  mb-3" id="img-3"><img src="./images/marc-home.jpg" alt="Certified Site Builder" class="certificate">
      </div>

      <p>&nbsp;</p>
      <hr/>

      </div>
    </div>
  
  </div>

  <!-- Accordion  -->
  <div class="accordion container" id="accordionExample">

  <?php
    $wowEffect = [
        'slideInUp',
        'slideInDown',
        'fadeIn',
        'bounceInUp',
        'rollIn',
    ];

    $html = "";
    $count  = 0;
    foreach ($accordionYoutub as $key => $value) {

      list($pre, $youtub_key) = explode("--", $key);

      $delay = rand(2, 10) / 10;
      $effect = $wowEffect[rand(0, 4)];

      $show = ($count == 0) ? " show" : "";
      $html =
      '<div class="card wow ' . $effect . '  my-3" data-wow-delay="' . $delay . 's">
              <div class="card-header" id="heading_' . $key . '">
                <h2 class="mb-0">
                  <a class="btn btn-link btn-block text-left" data-toggle="collapse" data-target="#' . $key . '" aria-expanded="true" aria-controls="collapseOne">
                    ' . $value . '
                  </a>
                </h2>
              </div>

              <div id="' . $key . '" class="collapse' . $show . '" aria-labelledby="heading_' . $key . '" data-parent="#accordionExample">
                <div class="card-body text-center">
                  ' . getYoutubeThumb($youtub_key) . '
                </div>
              </div>
            </div>';

      print $html;
      $count++;


    }

  ?>



  </div> <!-- END Accordion  -->


</article>