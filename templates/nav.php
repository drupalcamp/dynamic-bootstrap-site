<nav class="navbar navbar-light sticky-top">
  <div class="brand-navbar-icon">
  <a class="navbar-brand" href="/">The Real Gung Wang</a>
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  </div>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <div class="container-fluid">
      <div class="row">
        <ul class="navbar-nav col-sm-12 col-md-6">
          <li class="nav-item">
            <a class="vivaldi nav-link" href="<?Php echo $urls['vivaldi'] ?>"><?php echo $title['vivaldi']?> <span class="sr-only">(current)</span></a>
          </li>

          <li class="nav-item">
            <a class="shakespeare nav-link " href="<?php echo $urls['shakespeare'];?>">
              <?php echo $title['shakespeare']; ?></a></li>

          <li class="nav-item">
            <a class="scripts nav-link dropdown-toggle " href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Other Three Videos</a>

              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <ul class="mobile-submenu scripts">
                  <li class="nav-item menu-3-sub-1">
                    <a href="<?php echo $urls['mrh']; ?>" class="mrh nav-link"><?php echo $title['mrh']; ?></a>
                  </li>

                  <li class="nav-item menu-3-sub-2">
                    <a href="<?php echo $urls['cold']; ?>" class="cold nav-link"><?Php echo $title['cold']; ?></a>
                  </li>

                  <li class="nav-item menu-3-sub-3">
                    <a href="<?php echo $urls['juarez']; ?>" class="juarez nav-link"><?php echo $title['juarez'];?></a>
                  </li>

                </ul>
              </div>
          </li>
          <li class="nav-item">
            <a class="marc nav-link" href="<?php echo $urls['marc'];?>"><?php echo $title['marc']; ?></a>
            </li>
        </ul>
      
        <!-- Hover Right Box to show image & submenu  -->
        <div class="menu-hover-right col-sm-12 col-md-6">
          <div class="vivaldi hover-box-wapper menu-1" >
            <a href="<?php echo $urls['vivaldi']?>">
              <img src="<?php echo $thumbs['vivaldi']?>" class="vivaldi menu-img img-fluid" alt="<?php echo $title['vivaldi'];?>"><?php echo $title['vivaldi'];?>
            </a>
          </div>
          <div class="shakespeare hover-box-wapper menu-2" >
            <a href="<?php echo $urls['shakespeare']?>">
              <img src="<?php echo $thumbs['shakespeare']?>" class="shakespeare menu-img img-fluid" alt="<?php echo $title['shakespeare'];?>"><?php echo $title['shakespeare'];?>
            </a>
          </div>
          <div class="scripts hover-box-wapper menu-3" >
            <div class="mrh submenu-img">
              <img src="<?php echo $thumbs['mrh']?>" class="mrh menu-img img-fluid" alt="<?php echo $title['mrh'];?>">
            </div>
            <div class="cold submenu-img">
              <img src="<?php echo $thumbs['cold']?>" class="cold menu-img img-fluid" alt="<?php echo $title['cold'];?>">
            </div>
            <div class="juarez submenu-img">
              <img src="<?php echo $thumbs['juarez']?>" class="juarez menu-img img-fluid" alt="<?php echo $title['juarez'];?>">
            </div>

            <!-- <ul class="submenu">
              <li class="nav-item menu-3-sub-1">
                <a href="<?php echo $urls['mrh'];?>" class="mrh nav-link"><?php echo $title['mrh']; ?></a>
              </li>

              <li class="nav-item menu-3-sub-2">
                <a href="<?php echo $urls['cold'];?>" class="cold nav-link"><?php echo $title['cold']; ?></a>
              </li>

              <li class="nav-item menu-3-sub-3">
                <a href="<?php echo $urls['juarez'];?>" class="juarez nav-link"><?php echo $title['juarez']; ?></a>
              </li>
            </ul> -->


          </div>
          <div class="marc hover-box-wapper menu-4" >
            <a href="<?php echo $urls['marc']?>">
              <img src="<?php echo $thumbs['marc']?>" class="marc menu-img img-fluid" alt="<?php echo $title['marc'];?>"><?php echo $title['marc'];?>
            </a>
          </div>

        </div>

        <!-- END Hover Right Box to show image & submenu  -->
      </div>
    </div>
  </div>
</nav>
    <!-- 
      <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form> 
    -->