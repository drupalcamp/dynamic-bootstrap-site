<div id="homeSlideShow" class="carousel slide" data-ride="carousel"  data-pause="hover">
  <!-- 1st slide indicator -->
  <ol class="carousel-indicators row">
    <li data-target="#homeSlideShow" data-slide-to="0" class="active">
      <a href="" class="thumb-link">
        <div class="thumb-wrapper">
          <img src="<?php echo $thumbs['marc'];?>" alt="<?php echo $title['marc'];?>" class="thumb-img img-thumbnail">
          <div class="thumb-text"><h4><?php echo $title['marc'];?></div>
        </div>
      </a>

    </li>
    <!-- 2nd slide indicator -->
    <li data-target="#homeSlideShow" data-slide-to="1">
      <a href="" class="thumb-link">
        <div class="thumb-wrapper">
          <img src="<?php echo $thumbs['vivaldi'];?>" alt="<?php echo $title['vivaldi'];?>" class="thumb-img img-thumbnail">
          <div class="thumb-text"><h4><?php echo $title['vivaldi'];?></div>
        </div>
      </a>
    </li>
    <!-- 3rd slide indicator -->
    <li data-target="#homeSlideShow" data-slide-to="2">
      <a href="" class="thumb-link">
        <div class="thumb-wrapper">
          <img src="<?php echo $thumbs['shakespeare'];?>" alt="<?php echo $title['shakespeare'];?>" class="thumb-img img-thumbnail">
          <div class="thumb-text"><h4><?php echo $title['shakespeare'];?></div>
        </div>
      </a>
    </li>
    <!-- 4th slide indicator -->
    <li data-target="#homeSlideShow" data-slide-to="3">
      <a href="" class="thumb-link">
        <div class="thumb-wrapper">
          <img src="<?php echo $thumbs['mrh'];?>" alt="<?php echo $title['mrh'];?>" class="thumb-img img-thumbnail">
          <div class="thumb-text"><h4><?php echo $title['mrh'];?></div>
        </div>
      </a>
    </li>
    <!-- 5th slide indicator -->
    <li data-target="#homeSlideShow" data-slide-to="4">
      <a href="" class="thumb-link">
        <div class="thumb-wrapper">
          <img src="<?php echo $thumbs['cold'];?>" alt="<?php echo $title['cold'];?>" class="thumb-img img-thumbnail">
          <div class="thumb-text"><h4><?php echo $title['cold'];?></div>
        </div>
      </a>
    </li>
    <!-- 6th slide indicator -->
    <li data-target="#homeSlideShow" data-slide-to="5">
      <a href="" class="thumb-link">
        <div class="thumb-wrapper">
          <img src="<?php echo $thumbs['juarez'];?>" alt="<?php echo $title['juarez'];?>" class="thumb-img img-thumbnail">
          <div class="thumb-text"><h4><?php echo $title['juarez'];?></div>
        </div>
      </a>
    </li>
  </ol>

  <div class="carousel-inner">
    <!-- 1st slide -->
    <div class="carousel-item active">
      <img class="d-block w-100" src="<?php echo $images['marc'];?>" alt="First slide">
      <div class="carousel-caption  d-md-block slide1">
        <div class="count-number">Jun 13, 2020</div>
        <h1><?php echo $title['marc'];?></h1>
        <div class="teaser"><?php echo $summary['marc'];?></div>
        <a href="<?php echo $urls['marc'];?>">
          <div class="read-more">
            <span class="read-test">READ STORY</span><span class="right-arrow"></span></div>            
        </a>
      </div>
    </div>
     <!-- 2nd slide -->
    <div class="carousel-item">
      <img class="d-block w-100" src="<?php echo $images['vivaldi'];?>" alt="Second slide">
      <div class="carousel-caption  d-md-block slide2">
        <div class="count-number">Jun 13, 2020</div>
        <h1><?php echo $title['vivaldi'];?></h1>
        <div class="teaser"><?php echo $summary['vivaldi'];?></div>
        <a href="<?php echo $urls['vivaldi'];?>">
          <div class="read-more">
            <span class="read-test">READ STORY</span><span class="right-arrow"></span></div>            
        </a>
      </div>
    </div>
     <!-- 3rd slide -->
    <div class="carousel-item">
      <img class="d-block w-100" src="<?php echo $images['shakespeare'];?>" alt="Third slide">
      <div class="carousel-caption  d-md-block slide3">
        <div class="count-number">Jun 13, 2020</div>
        <h1><?php echo $title['shakespeare'];?></h1>
        <div class="teaser"><?php echo $summary['shakespeare'];?></div>
        <a href="<?php echo $urls['shakespeare'];?>">
          <div class="read-more">
            <span class="read-test">READ STORY</span><span class="right-arrow"></span></div>            
        </a>
      </div>
    </div>
     <!-- 4th slide -->
    <div class="carousel-item">
      <img class="d-block w-100" src="<?php echo $images['mrh'];?>" alt="Third slide">
      <div class="carousel-caption  d-md-block slide4">
        <div class="count-number">Jun 13, 2020</div>
        <h1><?php echo $title['mrh'];?></h1>
        <div class="teaser"><?php echo $summary['mrh'];?></div>
        <a href="<?php echo $urls['mrh'];?>">
          <div class="read-more">
            <span class="read-test">READ STORY</span><span class="right-arrow"></span></div>            
        </a>
      </div>
    </div>
     <!-- 5th slide -->
    <div class="carousel-item">
      <img class="d-block w-100" src="<?php echo $images['cold'];?>" alt="Third slide">
      <div class="carousel-caption  d-md-block slide5">
        <div class="count-number">Jun 13, 2020</div>
        <h1><?php echo $title['cold'];?></h1>
        <div class="teaser"><?php echo $summary['cold'];?></div>
        <a href="<?php echo $urls['cold'];?>">
          <div class="read-more">
            <span class="read-test">READ STORY</span><span class="right-arrow"></span></div>            
        </a>
      </div>
    </div>
     <!-- 6th slide -->
    <div class="carousel-item">
      <img class="d-block w-100" src="<?php echo $images['juarez'];?>" alt="Third slide">
      <div class="carousel-caption  d-md-block slide6">
        <div class="count-number">Jun 13, 2020</div>
        <h1><?php echo $title['juarez'];?></h1>
        <div class="teaser"><?php echo $summary['juarez'];?></div>
        <a href="<?php echo $urls['juarez'];?>">
          <div class="read-more">
            <span class="read-test">READ STORY</span><span class="right-arrow"></span></div>            
        </a>
      </div>
    </div>

  </div>

  <a class="carousel-control-prev" href="#homeSlideShow" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#homeSlideShow" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>

</div>
