<?php

$sysopsis = "Summary | 简介";
$subscribe = "https://www.youtube.com/channel/UCI9UQIXiP-BYqnFnup-QlTA?sub_confirmation=1";

function getYoutube($key)
{

  $str = '
  <iframe width="100%" height="280" src="https://www.youtube.com/embed/' . $key . '" frameborder="4" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
  </iframe>
  ';
    return $str;
}

function getYoutubeBS($key) {

  $str = '
    <div class="embed-responsive embed-responsive-16by9">
      <iframe clasiframes="embed-responsive-item" src="https://www.youtube.com/embed/' . $key . '?rel=0" allowfullscreen allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"></iframe>
    </div>';

  return $str;
}


function getYoutubeThumb($key) {
  $urlImg="https://img.youtube.com/vi/$key/0.jpg";
  $urlVideo = "https://www.youtube.com/embed/$key";

  $html = '
    <a href="' . $urlVideo . '" data-key="' . $key . '" class="play-youtube-img">
    <img src="'. $urlImg . '" alt="Youtube ' . $key . '" class="play-youtube-video img-thumbnail" async> 
    <img src="/images/icons/play.svg" alt="Play Youtube" class="player-button ">
    </a>';

  return $html;
}

$title = [
    'home' => "Home",
    'marc' => "Gung Wang",
    'vivaldi' => "Build & Update Drupal 8/9 on Ubuntu-18",
    'shakespeare' => "Host Drupal 8 Website on Home PC",
    'mrh' => "Build Poll Application on Drupal 8",
    'cold' => "Teamwork with GIT & Sync Config on Drupal 8",
    'juarez' => "Use Domain, BVG & Context modules on Drupal 8",
    'hertzog' => "Set up Advanced Solr Search on Drupal 8",
    'ocracoke' => "Build Ubuntu Linux Server on VMware VM",
    'whn' => "Build Drupal 8 Module from Scratch",
    'term' => "",
    'other' => "",
];

$shortTitle = [
    'home' => "Home",
    'marc' => "About Me",
    'vivaldi' => "Drupal on Ubuntu",
    'shakespeare' => "Host Drupal @Home",
    'mrh' => "Poll Application",
    'cold' => "GIT & Sync Config",
    'juarez' => "Domain & Context Modules",
    'hertzog' => "Advanced Solr Search",
    'ocracoke' => "Ubuntu on VMware",
    'whn' => "Build Module",
    'term' => "",
    'other' => "",
];

$urls = [
    'home' => "./",
    'marc' => "./?q=marc",
    'vivaldi' => "./?q=vivaldi",
    'shakespeare' => "./?q=shakespeare",
    'mrh' => "./?q=mrh",
    'cold' => "./?q=cold",
    'juarez' => "./?q=juarez",
    'hertzog' => "./?q=hertzog",
    'ocracoke' => "./?q=ocracoke",
    'whn' => "./?q=whn",
    'term' => "./?q=term",
    'other' => "./?q=",
];

$summary = [
    'home' => "Senior Web Engineer, <br>  Drupal-8 Certified Front-end Specialist, Developer & Site Builder.",

    'marc' => "Senior Web Engineer with Master's Degree in Computer Sciences. He is  Drupal 8 Certified Front-end Specialist, Certified Developer, and Site Builder. <br>(高级网站工程师，硕士学位，Drupal-8 认证前端专家、认证开发者和认证网站建设者）",

    'vivaldi' => "How to install and set up Drupal 8 on Ubuntu Linux VMware?<br>
  如何在虚拟机（Ubuntu Linux）上升级Drupal 8 & 9？如何卸载PHP7.4并降级到PHP7.3？如何解决Drush、Drupal、PHP的版本兼容问题？",

    'mrh' => "How to Build a Poll App on Drupal 8?<br>
  如何建设开发新网站投票系统 Poll（合集）？学习前端开发：JQuery，样式设计SCSS, Twig Template, Poll模块, Druapl 8/9 ",

    'cold' => "Avoid Code Overriding in Teamwork (GIT, Drush, Composer, Sync Config)?<br>如何避免源代号丢失和配置被覆盖的问题？如何合并代码，同步配置文件，D8, GIT, Sync config?",

    'hertzog' => "How to Set up Solr Server and Build Solr Indexing Service on Drupal 8?  <br>
  如何在Drupal8中创建SOLR搜索服务？如何利用Pantheon的Solr服务，从简单到高级功能，一步一步配置和优化？
  ",

    'juarez' => "How to Use Domain, BVG & Context Modules to Build Multiple Domains/Subdomains Drupal 8 Site?<br>
  学习 Domain、BVG、Context模块，实现多域名控制. 如何实现一个代号源、一个数据库、多个域名导航？  ",

    'ocracoke' => "How to Install/Config Ubuntu Linux on VMware VM? How to Build Web Server on Apache, PHP, MariaDB & Drupal 8?<br>
  如何安装、配置VMware虚拟机、网站服务器和Drupal？VM, Ubuntu Linux, PHP, MariaDB, Drupal 8。",

    'whn' => "How Build/Implement a Custom Module of Drupal 8: Detecting IP/GEO Info.<br>
  如何从零开始开发DUPAL8模块？获得用户IP及GEO地理信息、并设置相关信息| 从后端PHP到前端Twig、CSS、JavaScript
  ",

    'shakespeare' => "How to host Domain on Home PC for Drupal 8 site?<br>
  如何设置域名（Domain）指向到家用电脑上，以省去网络托管的支出？如何设置家用局域网使域名定向到特定的一台电脑上？如何设置VMware虚拟机财网络和Ubuntu 18 Linux？
  ",

    'term' => "ypopulus vereor vulpes. Aliquip blandit cogo nobis pneum praesent tum. Abdo in natu ullamcorper usitas. Dolus gravis humo sed te.",

    'other' => "Mminim populus vereor vulpes. Aliquip blandit cogo nobis pneum praesent tum. Abdo in natu ullamcorper usitas. Dolus gravis humo sed te.",
];

$bgClass = [
    'home' => "bg-home",
    'marc' => "bg-marc",
    'vivaldi' => "bg-vivaldi",
    'shakespeare' => "bg-shakespeare",
    'mrh' => "bg-mrh",
    'cold' => "bg-cold",
    'juarez' => "bg-juarez",
    'hertzog' => "bg-hertzog",
    'ocracoke' => "bg-ocracoke",
    'whn' => "bg-whn",
    'term' => "bg-term",
    'other' => "bg-other",
];

/* <?php echo $thumbs[''];?> */

$thumbs = [
    'home' => "",
    'marc' => "./images/marc-thumb.jpg",
    'vivaldi' => "./images/vivaldi-thumb.jpg",
    'shakespeare' => "./images/shakespeare-thumb.jpg",
    'mrh' => "./images/mrh-thumb.jpg",
    'cold' => "./images/cold-thumb.jpg",
    'juarez' => "./images/juarez-thumb.jpg",
    'hertzog' => "./images/hertzog-thumb.jpg",
    'ocracoke' => "./images/ocracoke-thumb.jpg",
    'whn' => "./images/whn-thumb.jpg",
    'term' => "",
    'other' => "",
];

/* <?php echo $images[''];?> */
$images = [
    'home' => "",
    'marc' => "./images/home.png",
    'vivaldi' => "./images/vivaldi-home.jpg",
    'shakespeare' => "./images/shakespeare-home.jpg",
    'mrh' => "./images/mrh-home.jpg",
    'cold' => "./images/cold-home.jpg",
    'juarez' => "./images/juarez-home.jpg",
    'hertzog' => "./images/hertzog-home.jpg",
    'ocracoke' => "./images/ocracoke-home.jpg",
    'whn' => "./images/whn-home.jpg",
    'term' => "",
    'other' => "",
];


$accordionYoutub = [
  'yt--uNG3xPBm4Lg' => "1) 一步步学习开发建设自适应动态网站（PHP, JS, HTML5, Bootstrap, Gulp SCSS, Dynamic Responsive Website）第一集，简介、准备和网站的设计",
  'yt--zqvnFZqu6Vo' => "2) 一步步学习开发建设自适应动态网站（PHP, JS, HTML5, Bootstrap, Gulp SCSS, Dynamic Responsive ）第二集，写代码以创建导航菜单",
  'yt--9iIUYh6p6nA' => "3.1)一步步学习开发建设自适应动态网站（PHP, JS, HTML5, Bootstrap, Gulp SCSS, Dynamic Responsive ）第三集（上），创建动态导航菜单",
  'yt--ZRoXVB3aCJY' => "3.2)一步步学习开发建设自适应动态网站（PHP, JS, HTML5, Bootstrap, Gulp SCSS, Dynamic Responsive ）第三集（下），完善动态导航菜单",
  'yt--1vmqpAChs2U' => "4.1)一步步学习开发建设自适应动态网站（PHP, JS, HTML5, Bootstrap, SCSS, Responsive ）第四集（上），使用Bootstrap4创建幻灯片Carousel",
  'yt--Go4kxTeGf84' => "4.2)一步步学习开发建设自适应动态网站（PHP, JS, HTML5, Bootstrap, SCSS, ）第四集（下）设置Gitlab管理代码, 高级定制幻灯片Carousel, 样式设计",
  'yt--kCpd4cYDVgk' => "5)一步步学习开发建设自适应动态网站（PHP, JS, HTML5, Bootstrap, SCSS, ）第五集，修改主页以动态加载子网页、创建子网页模板、样式设计",
  'yt--uxQ4sd29764' => "6)一步步学习开发建设自适应动态网站（PHP, HTML5, Bootstrap, WOW JS, SASS ）第六集，运用BootStrap、WOW 创建 Cards 动态模板",
  'yt--dn3brLLeHcg' => "7.1)一步步学习开发建设自适应动态网站（PHP, HTML5, Bootstrap, WOW JS, SASS ）第七集（上）运用BootStrap 创建好看的社交平台图标 Social Icons",
  'yt--RKhjmJ_w9g8' => "7.2) 一步步学习开发建设自适应动态网站（PHP, HTML5, Bootstrap, SASS ）第七集（下）运用Bootstrap创建坍方、手风琴 | Collapes, Accordion",
  'yt--8IKDHjtTZio' => "8.1) 一步步学习开发建设自适应动态网站（PHP, HTML5, Bootstrap, SASS ）第八集（上）动态延迟加载YouTube视频 | Lazy Loading Youtube",
  'yt--dTozolY33mE' => "8.2) 一步步学习开发建设自适应动态网站（PHP, HTML5, Bootstrap, SASS ）第八集（下）动态延迟加载YouTube，点击标题关闭视频 | Stop YouTube",
  'yt--LQOiIyTgHEE' => "9) 一步步学习开发建设自适应动态网站（PHP, HTML5, Bootstrap, SASS ）第九集，调试和修改移动设备网页的样式，Mobile Style，Bootstrap",
  ];