<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equuiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,800" >

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto Slab'>

      <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Material Design Bootstrap -->
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="src/css/bootstrap.css">
    <link rel="stylesheet" href="src/css/animate.css">
    <!-- <link rel="stylesheet" href="src/MDB-Free/css/mdb.min.css"> -->

    <link rel="stylesheet" href="src/css/styles.css">

    <title >Dynamic Demo</title>

  </head>
  <?php require_once("./templates/varaibles.php");?>
  <body>

    <!-- <h1>Hello, world!</h1> -->
  <header>

  <?php 
  $isFront = isset($_GET['q']) ? false : true ;
  $template = "";
  
  if ($isFront) {
    require_once("./templates/nav.php"); 
  }
  else {
    require_once "./templates/nav-sub.php";

  }
  
  ?>
  </header>
  <main>

    <?php 
    // http: //dynamic.test/?q=marc
    if(isset($_GET['q'])) {
      $template = "./templates/" . $_GET['q']. ".php";
      if(file_exists($template)) {
        $isFront = false;
        require_once($template);
      }
    }
    else {
      $isFront = true;
    }

    if($isFront === true) {      
      require_once("./templates/carousel.php");
      require_once("./templates/homeBody.php");
    }

    ?>
  </main>

  <footer class="bg-light">
    <?php require_once("./templates/footer.php"); ?>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="src/js/jquery.min.js" ></script>
    <script src="src/js/popper.min.js" ></script>
    <script src="src/js/bootstrap.min.js" ></script>
    <!-- MDB core JavaScript -->
    <!-- <script type="text/javascript" src="src/MDB-Free/js/mdb.min.js"></script> -->

    <script src="src/js/wow.min.js"></script>
    <script>
      wow = new WOW(
      {
        boxClass:     'wow',      // default
        animateClass: 'animated', // default
        offset:       3,          // default
        mobile:       true,       // default
        live:         true        // default
      }
      )
      wow.init();
    </script>
    <script src="src/js/index.js" ></script>
    

    <div class="footer-js">

    </div>
  </footer>

  <button id="myBtn" class="" title="Go to top">Top</button>
  </body>



